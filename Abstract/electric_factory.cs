using filing;
using FactoryMethod;
class electric_factory : Fabric{

    public Vehicle build_car()
    {
   
        return new electric_car();
    }

   public Vehicle build_bike()
    {

        return new electric_bike();
    }

   public Vehicle build_motorcycle()
    {

        return new electric_motorcycle();
    }
    

}