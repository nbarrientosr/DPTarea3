﻿using FactoryMethod;

namespace filing
{
    public interface Fabric
    {
        Vehicle build_motorcycle();
        Vehicle build_bike();
        Vehicle build_car();

    }
}