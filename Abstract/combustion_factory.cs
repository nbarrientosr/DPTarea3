using FactoryMethod;
using filing;

class combustion_factory : Fabric{

   public  Vehicle build_car()
    {
   
        return new petrol_car();
    }

   public  Vehicle build_bike()
    {

        return new petrol_bike();
    }

   public Vehicle build_motorcycle()
    {

        return new petrol_motorcycle();
    }
}