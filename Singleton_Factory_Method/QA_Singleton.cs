using System;
using GlobalVar;

namespace QA_Singleton
{
    public class Singleton
    {
        private Singleton() { }
        private static Singleton ?_instance = null;
        public static Singleton GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Singleton();
            }
            return _instance;
        }
        public string QA_Avanzar()
        {
            return "Avanzando";
        }
        public string QA_Acelerar()
        {
            return "Acelerando";
        }
        public string QA_Frenar()
        {
            return "Frenando";
        }
        public string QA_Retroceder()
        {
            return "Retrocediendo";
        }
    }
    class Singleton_Client
    {
        public void Main()
        {     
            Console.WriteLine("Creando instancia 1...");      
            var s1 = Singleton.GetInstance();
            Console.WriteLine("Creando instancia 2...");      
            var s2 = Singleton.GetInstance();
            bool testing = true;
            while(testing == true)
            {
                Console.WriteLine ("--- Pruebas ---");
                Console.WriteLine ("1. Acelerar.");
                Console.WriteLine ("2. Avanzar.");
                Console.WriteLine ("3. Frenar.");
                Console.WriteLine ("4. Retroceder.");
                Console.WriteLine ("5. Salir.");
                int testOpcion = int.Parse(Console.ReadLine());
                if (testOpcion == 1)
                {
                    Console.WriteLine(s2.QA_Acelerar());
                    Globals.counter = Globals.counter + 1;
                }
                else if (testOpcion == 2)
                {
                    Console.WriteLine(s2.QA_Avanzar());
                    Globals.counter = Globals.counter + 1;
                }
                else if (testOpcion == 3)
                {
                    Console.WriteLine(s2.QA_Frenar());
                    Globals.counter = Globals.counter + 1;
                }
                else if (testOpcion == 4)
                {
                    Console.WriteLine(s2.QA_Retroceder());
                    Globals.counter = Globals.counter + 1;
                }
                else if (testOpcion == 5)
                {
                    testing = false;
                    Console.WriteLine("Saliendo...");
                }
                if (Globals.counter % 5 == 0)
                {
                    SaveInfo();
                }
            }
        }
        static void SaveInfo()
        {
            try
            {
                DateTime localDate = DateTime.Now;
                StreamWriter sw = new StreamWriter("QAData.txt", true);
                string data = "pruebas_";
                sw.WriteLine(data + localDate);
                sw.Close();
                Console.WriteLine("Datos guardados con exito");
            }            
            catch (Exception e)
            {
                Console.WriteLine("Error al guardar los datos en el archivo: " + e.Message); 
            }
        }
    }
}