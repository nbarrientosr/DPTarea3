using System;

namespace AbstractFactory
{
    public interface IFactory
    {
        IMotorcycle CreateMotorcycle();
        IBicylcle CreateBicycle();
        ICar CreateCar();
    }
    class ElectricFactory : IFactory
    {
        public IMotorcycle CreateMotorcycle()
        {
            return new ElectricMotorcycle();
        }
        public IBicylcle CreateBicycle()
        {
            return new ElectricBicycle();
        }
        public ICar CreateCar()
        {
            return new ElectricCar();
        }
    }
    class GasFactory : IFactory
    {
        public IMotorcycle CreateMotorcycle()
        {
            return new GasMotorcycle();
        }
        public IBicylcle CreateBicycle()
        {
            return new GasBicycle();
        }
        public ICar CreateCar()
        {
            return new GasCar();
        }
    }
    public interface IMotorcycle
    {
        string messageMotorcycle();
    }
    class ElectricMotorcycle : IMotorcycle
    {
        public string messageMotorcycle()
        {
            return "Se ha creado una motocicleta electrica.";
        }
    }
    class GasMotorcycle : IMotorcycle
    {
        public string messageMotorcycle()
        {
            return "Se ha creado una motocicleta de gasolina.";
        }
    }
    public interface IBicylcle
    {
        string messageBicycle();
    }
    class ElectricBicycle : IBicylcle
    {
        public string messageBicycle()
        {
            return "Se ha creado una bicicleta electrica.";
        }
    }
    class GasBicycle : IBicylcle
    {
        public string messageBicycle()
        {
            return "Se ha creado una bicicleta de gasolina.";
        }
    }
    public interface ICar
    {
        string messageCar();
    }
    class ElectricCar : ICar
    {
        public string messageCar()
        {
            return "Se ha creado un automovil electrico.";
        }
    }
    class GasCar : ICar
    {
        public string messageCar()
        {
            return "Se ha creado un automovil de gasolina.";
        }
    }
    class AbstractFactory_Client
    {
        public void Main()
        {
            Console.WriteLine("Seleccione una opcion: ");
            Console.WriteLine("1. Motor Electrico");
            Console.WriteLine("2. Motor de Combustion");
            int optionAF1 = int.Parse(Console.ReadLine());
            if (optionAF1 == 1)
            {
                Console.WriteLine("Se va a construir un vehiculo con motor electrico.");
                ClientMethod(new ElectricFactory());
            }
            else if (optionAF1 == 2)
            {
                Console.WriteLine("Se va a construir un vehiculo con motor de combustion.");
                ClientMethod(new GasFactory());
            }
        }
        public void ClientMethod(IFactory factory)
        {
            Console.WriteLine("Seleccione una opcion: ");
            Console.WriteLine("1. Motocicleta");
            Console.WriteLine("2. Bicicleta");
            Console.WriteLine("3. Automovil");
            int optionAF2 = int.Parse(Console.ReadLine());
            if (optionAF2 == 1)
            {
                var newMotorcycle = factory.CreateMotorcycle();
                Console.WriteLine(newMotorcycle.messageMotorcycle());
            }
            else if (optionAF2 == 2)
            {
                var newBicycle = factory.CreateBicycle();
                Console.WriteLine(newBicycle.messageBicycle());
            }
            else if (optionAF2 == 3)
            {
                var newCar = factory.CreateCar();
                Console.WriteLine(newCar.messageCar());
            }
        }
    }
}