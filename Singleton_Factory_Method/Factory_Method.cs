using System;

namespace FactoryMethod
{
    abstract class Genesis
    {
        public abstract Vehicle FactoryMethod();

        public string SomeOperation()
        {
            var product = FactoryMethod();
            var result = product.Agregado();
            return result;
        }
    }

    class ElectricGenesis : Genesis
    {
        public override Vehicle FactoryMethod()
        {
            return new ElectricMotor();
        }
    }

    class CombustionGenesis : Genesis
    {
        public override Vehicle FactoryMethod()
        {
            return new CombustionEngine();
        }
    }

    public interface Vehicle
    {
        string Agregado();
    }

    class ElectricMotor : Vehicle
    {
        public string Agregado()
        {
            return "Se ha agregado un motor electrico al vehiculo.";
        }
    }

    class CombustionEngine : Vehicle
    {
        public string Agregado()
        {
            return "Se ha agregado un motor de combustion al vehiculo.";
        }
    }

    class Client
    {
        public void Main()
        {
            Console.WriteLine("Seleccione una opcion: ");
            Console.WriteLine("1. Motor Electrico");
            Console.WriteLine("2. Motor de Combustion");
            int optionFM = int.Parse(Console.ReadLine());
            if (optionFM == 1)
            {
                Console.WriteLine("Se va a construir un vehiculo con motor electrico.");
                ClientCode(new ElectricGenesis());
            }
            else if (optionFM == 2)
            {
                Console.WriteLine("Se va a construir un vehiculo con motor de combustion.");
                ClientCode(new CombustionGenesis());
            }
        }

        public void ClientCode(Genesis creator)
        {
            Console.WriteLine(creator.SomeOperation());
        }
    }
}