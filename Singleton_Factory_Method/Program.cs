﻿using System;
using  QA_Singleton;
using FactoryMethod;
using GlobalVar;
using System.IO;
using AbstractFactory;

public class DesignPatternsT3
{
    public static void Main(string[] args)
    {
        MenuPrincipal();
    }


    static void MenuPrincipal()
    {
        bool keepLoop = true;
        while (keepLoop == true)
        {
            Console.WriteLine (" ");
            Console.WriteLine ("------------");
            Console.WriteLine ("¡BIENVENIDO!");
            Console.WriteLine ("1. Test Singleton.");
            Console.WriteLine ("2. Test Factory Method.");
            Console.WriteLine ("3. Test Abstract Factory.");
            Console.WriteLine ("4. Exit.");
            int option = int.Parse(Console.ReadLine());
            switch (option)
            {
                case 1:
                    new Singleton_Client().Main();
                    break;

                case 2:
                    new FactoryMethod_Client().Main();
                    break;

                case 3:
                    new AbstractFactory_Client().Main();
                    break;

                case 4:
                    Environment.Exit(0);
                    break;

                default:
                    Console.WriteLine("Opción inválida. Intente nuevamente.");
                    break;
            }            
        }
    }
}